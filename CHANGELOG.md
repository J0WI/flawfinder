# Flawfinder analyzer changelog

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!17)

## v2.3.0
- Add support for custom CA certs (!15)

## v2.2.1
- Use Debian Stretch as base image (!11)

## v2.2.0
- Bump Flawfinder to [2.0.10](https://sourceforge.net/p/flawfinder/code/ci/2.0.10/tree/ChangeLog)

## v2.1.0
- Bump Flawfinder to [2.0.8](https://sourceforge.net/p/flawfinder/code/ci/2.0.8/tree/ChangeLog)

## v2.0.1
- Bump common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.4.0
- Add an `Identifier` generated from the Flawfinder's function name

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
