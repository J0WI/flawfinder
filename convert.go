package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	columnFile = iota
	columnLine
	columnColumn
	columnLevel
	columnCategory
	columnName
	columnWarning
	columnSuggestion
	columnNote
	columnCWEs
	columnContext
	columnFingerprint

	scannerID   = "flawfinder"
	scannerName = "Flawfinder"
)

func convert(r io.Reader, prependPath string) (*issue.Report, error) {
	reader := csv.NewReader(r)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	report := issue.NewReport()

	if len(records) < 2 {
		return &report, nil // empty document
	}
	records = records[1:]

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	issues := make([]issue.Issue, len(records))
	for i, sliceOfStrings := range records {
		record := Record(sliceOfStrings)
		r := Result{record, prependPath}
		issues[i] = issue.Issue{
			Category:    issue.CategorySast,
			Scanner:     scanner,
			Message:     r.Warning(),
			CompareKey:  r.CompareKey(),
			Confidence:  r.Confidence(),
			Location:    r.Location(),
			Identifiers: r.Identifiers(),
			Solution:    r.Suggestion(),
		}
	}

	report.Vulnerabilities = issues
	return &report, nil
}

// Result describes a result with a relative path.
type Result struct {
	Record
	PrependPath string
}

// Filepath returns the relative path of the affected file.
func (r Result) Filepath() string {
	return filepath.Join(r.PrependPath, r.File())
}

// CompareKey returns a string used to establish whether two issues are the same.
func (r Result) CompareKey() string {
	fingerprint, cwes := r.Record[columnFingerprint], r.Record[columnCWEs]
	return strings.Join([]string{r.Filepath(), fingerprint, cwes}, ":")
}

// Record wraps a line of Flawfinder's output.
type Record []string

// File returns the path of the affected file.
func (r Record) File() string {
	return r[columnFile]
}

// Line returns the line number of the affected code.
func (r Record) Line() int {
	line, _ := strconv.Atoi(r[columnLine])
	return line
}

// Name returns the function name (id) of the affected code.
func (r Record) Name() string {
	return r[columnName]
}

// CWEs return a list of CWE IDs based on the columnCWEs which supports multiple formats:
// "CWE-119" => the vulnerability is mapped to CWE-119.
// "CWE-119, CWE-120" => the vulnerability is mapped to both CWE-119 and CWE-120.
// "CWE-119!/CWE-120" => the vulnerability is mapped to CWE-119 and CWE-120 is a subset of CWE-119.
// "CWE-362/CWE-367!" => the vulnerability is mapped to CWE-367 and CWE-367 is a subset of CWE-362.
func (r Record) CWEs() []int {
	rawCWEs := strings.FieldsFunc(r[columnCWEs], func(r rune) bool {
		return r == ',' || r == '/'
	})
	cwes := make([]int, len(rawCWEs))
	for i, cwe := range rawCWEs {
		// Remove bang, space and 'CWE-' prefix to have ID only
		id, err := strconv.Atoi(strings.Trim(cwe, "! CWE-"))
		if err != nil {
			continue
		}
		cwes[i] = id
	}

	return cwes
}

// Warning returns a warning message that explains the vulnerability.
func (r Record) Warning() string {
	return r[columnWarning]
}

// Suggestion returns a message on how to fix the vulnerability.
func (r Record) Suggestion() string {
	return r[columnSuggestion]
}

// Location returns a structured Location
func (r Result) Location() issue.Location {
	return issue.Location{
		File:      r.Filepath(),
		LineStart: r.Line(),
	}
}

// Confidence returns the normalized confidence
// See https://sourceforge.net/p/flawfinder/code/ci/2.0.5/tree/flawfinder
func (r Record) Confidence() issue.ConfidenceLevel {
	level, err := strconv.Atoi(r[columnLevel])
	if err != nil {
		return issue.ConfidenceLevelUnknown
	}

	// "risk_level" goes from 0 to 5. 0=no risk, 5=maximum risk
	switch level {
	case 4, 5:
		return issue.ConfidenceLevelHigh
	case 3:
		return issue.ConfidenceLevelMedium
	case 1, 2:
		return issue.ConfidenceLevelLow
	case 0:
		return issue.ConfidenceLevelIgnore
	}
	return issue.ConfidenceLevelUnknown
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (r Record) Identifiers() []issue.Identifier {
	identifiers := []issue.Identifier{
		r.FlawfinderIdentifier(),
	}

	for _, cwe := range r.CWEs() {
		identifiers = append(identifiers, issue.CWEIdentifier(cwe))
	}

	return identifiers
}

// FlawfinderIdentifier returns a structured Identifier for a Flawfinder Name
func (r Record) FlawfinderIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "flawfinder_func_name",
		Name:  fmt.Sprintf("Flawfinder - %s", r.Name()),
		Value: r.Name(),
	}
}
