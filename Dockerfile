FROM python:3-stretch

ARG FLAWFINDER_VERSION=2.0.10

RUN pip install flawfinder==$FLAWFINDER_VERSION
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
